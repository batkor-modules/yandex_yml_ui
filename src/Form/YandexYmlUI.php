<?php

namespace Drupal\yandex_yml_ui\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Yandex price list form.
 *
 */
class YandexYmlUI extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('yandex_yml_ui.config');
    $form['store_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Store'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['store_group']['name'] = [
      '#title' => $this->t('Store name'),
      '#type' => 'textfield',
      '#default_value' => $config->get('shop.name'),
    ];

    $form['store_group']['company'] = [
      '#title' => $this->t('Company name'),
      '#type' => 'textfield',
      '#default_value' => $config->get('shop.company'),
    ];

    $this->currenciesElements($form);

    $form['categories_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Categories'),
      '#open' => TRUE,
    ];

    $this->offersElements($form);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('yandex_yml_ui.config');
    $config
      ->set('shop', $form_state->getValue('store_group'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['yandex_yml_ui.config'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'yandex_yml_ui';
  }

  protected function offersElements(&$form) {
    $form['offers_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Offers'),
      '#open' => TRUE,
    ];
    
  }

  /**
   * Add currencies elements in form.
   *
   * @param $form
   */
  protected function currenciesElements(&$form) {
    $form['currencies_group'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Currencies settings'),
      '#open' => TRUE,
    ];

    $config = $this->config('yandex_yml_ui.config');
    $currencies = $config->get('currencies') ? $config->get('currencies') : [];
    foreach ($currencies as $key => $currency) {
      $items[] = [
        '#markup' => $this->t('Currency: @id, Rate: @rate. <br>', [
          '@id' => $currency['id'],
          '@rate' => $currency['rate'],
        ]),
        'link' => [
          '#type' => 'link',
          '#title' => $this->t('Edit'),
          '#url' => Url::fromRoute('yandex_yml_ui.currency', [], ['query' => ['type' => 'edit', 'key' => $key]]),
          '#attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-renderer' => 'off_canvas',
            'data-dialog-type' => 'dialog',
          ],
        ],
      ];
    }

    $form['currencies_group']['list'] = [
      '#theme' => 'item_list',
      '#items' => isset($items) ? $items : ['#markup' => 'Currencies unsed'],
    ];

    $form['currencies_group']['add_currency'] = [
      '#type' => 'link',
      '#title' => $this->t('Add currency'),
      '#url' => Url::fromRoute('yandex_yml_ui.currency', [], ['query' => ['type' => 'add']]),
      '#attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-renderer' => 'off_canvas',
        'data-dialog-type' => 'dialog',
      ],
      '#attached' => [
        'library' => [
          'core/drupal.dialog.ajax',
        ],
      ],
    ];
  }

}
