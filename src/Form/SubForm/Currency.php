<?php

namespace Drupal\yandex_yml_ui\Form\SubForm;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure Yandex yml UI settings for this site.
 */
class Currency extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_yml_ui_currency';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $id = '';
    $rate = '';
    $submit_label = $this->t('Add currency');

    $type = $this->getRequest()->get('type');
    $form_state->set('type', $type);
    if ($type == 'edit') {
      $key = $this->getRequest()->get('key');
      $form_state->set('key', $key);

      $config = $this->configFactory()->get('yandex_yml_ui.config');
      $currencies = $config->get('currencies') ? $config->get('currencies') : [];
      if (isset($currencies[$key])) {
        $id = $currencies[$key]['id'];
        $rate = $currencies[$key]['rate'];
      }

      $submit_label = $this->t('Change currency');
    }

    $form['id'] = [
      '#title' => $this->t('Currency code'),
      '#type' => 'textfield',
      '#default_value' => $id,
    ];

    $form['rate'] = [
      '#title' => $this->t('Currency rate'),
      '#type' => 'textfield',
      '#default_value' => $rate,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $submit_label,
    ];

    if ($type == 'edit') {
      $form['delete'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove currency'),
        '#submit' => ['::removeCurrency'],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $currencies = $config->get('currencies') ? $config->get('currencies') : [];
    $key = $form_state->get('key');
    if ($form_state->get('type') == 'edit') {
      if (isset($currencies[$key])) {
        $currencies[$key] = [
          'id' => $form_state->getValue('id'),
          'rate' => $form_state->getValue('rate'),
        ];
      }
    }
    else {
      $currencies[] = [
        'id' => $form_state->getValue('id'),
        'rate' => $form_state->getValue('rate'),
      ];
    }

    $config->set('currencies', $currencies)->save();
    $form_state->setRedirectUrl(Url::fromRoute('yandex_yml_ui.config'));
  }

  public function removeCurrency(array &$form, FormStateInterface $form_state) {
    $config = $this->getConfig();
    $currencies = $config->get('currencies') ? $config->get('currencies') : [];
    $key = $form_state->get('key');
    if (isset($currencies[$key])) {
      unset($currencies[$key]);
    }
    $config->set('currencies', $currencies)->save();
    $form_state->setRedirectUrl(Url::fromRoute('yandex_yml_ui.config'));
  }

  protected function getConfig() {
    return $this->configFactory()->getEditable('yandex_yml_ui.config');
  }

}
